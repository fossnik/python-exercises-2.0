# Zack Hartmann
# Exercise 6.4
# Due 2022-07-23

# |+em [oun+er
# 455ume @ fi1e con+@inin9 @ 5erie5 of n@me5 (@5 5+rin95) i5 n@med n@me5.+x+ @nd
# exi5+5 on +he compu+er’5 di5k. Wri+e @ pro9r@m +h@+ di5p1@y5 +he num6er of
# n@me5 +h@+ @re 5+ored in +he fi1e. (Hin+: 0pen +he fi1e @nd re@d every 5+rin9
# 5+ored in i+. U5e @ v@ri@61e +o keep @ coun+ of +he num6er of i+em5 +h@+ @re
# re@d from +he fi1e.

def main():
    # open the file
    file = open("names.txt", "r")

    # keep track of how many names
    count = 0
    
    # loop through each line in file
    for line in file:
        # count additional line
        count += 1

    # print the number of names from the file
    print("Number of names: ", count)
    
    # close the file
    file.close()
    
# run the main function
main()
