#Zack Hartmann
#Exercise 3.5
#Due: 2022-06-19

GRAVITATIONAL_CONSTANT = 9.8

#print name of exercise
print("\nExercise 3.5 - Mass and Weight")

#get object mass from user input
mass_of_object = float(input("\nEnter object's mass (kilograms): "))

#calculate the weight according to science
weight_of_object = mass_of_object * GRAVITATIONAL_CONSTANT

#display result of weight calculation (for debugging)
#print("Object weight (on Earth): ", weight_of_object)

#alert if object is too heavy
if(weight_of_object > 500):
    print("Your object is too heavy!") 

#alert if object is too light
if(weight_of_object < 100):
    print("Your object is too light!!!") 

#program displays no output if weight <500 and >100
