# Zack Hartmann
# Exercise 7.6
# Due 2022-08-04

# l4r63r 7h4n n
# 1n 4 pr06r4m, wr173 4 func710n 7h47 4cc3p75 7w0 4r6um3n75: 4 l157, 4nd 4
# numb3r n. 455um3 7h47 7h3 l157 c0n741n5 numb3r5. 7h3 func710n 5h0uld d15pl4y
# 4ll 0f 7h3 numb3r5 1n 7h3 l157 7h47 4r3 6r3473r 7h4n 7h3 numb3r n.

### validate user input (numbers only)
##def get_a_number():
##    return_value = "null"
##    # keep prompting till valid input
##    while return_value == "null":
##        try:
##            user_input = input(f"Enter a number: ")
##            # non-numerical input fails here
##            return_value = float(user_input)
##        except:
##            print("Invalid input")
##    return return_value
##    
### get 20 positive numbers
##def get_twenty_numbers():
##    # to store the 20 values
##    twenty_numbers = []
##    
##    # prompt user for 20 numbers
##    for number in range(20):
##        user_input = get_a_number()
##        twenty_numbers.append(user_input)
##
##    return twenty_numbers

# I could have done something like the above, but the instructions did not
# imply that user input should be taken. So I have used variables.
A_LIST = [1,2,3,4,5,6,67,87,12,9000,-54,0]
NUMBER_N = 5

def greater_than_n_from_list(list_of_numbers, n):
    # a list comprehension to get all values greater than n
    values_greater_than_n = [item for item in list_of_numbers if item > n]

    # a not-a-list-comprehension way to do the same thing
##    values_greater_than_n = []
##    for item in list_of_numbers:
##        if item > n:
##            values_greater_than_n.append(item)
    
    return values_greater_than_n

def main():
##    # get twenty numbers from user
##    numbers_list = get_twenty_numbers()
    numbers_list = A_LIST

    # prints out a list of numbers that has been defined
    print("Numbers list: ", numbers_list)

    # get the value of 'n'
    n = NUMBER_N

    # get and print out values larger than 'n'
    high_values = greater_than_n_from_list(numbers_list, n)
    print(f"Values greater than 'n' where n = {n}:\n", high_values)

# run the main function
main()
