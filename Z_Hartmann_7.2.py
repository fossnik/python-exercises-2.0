# Zack Hartmann
# Exercise 7.2
# Due 2022-07-31

# l0773ry numb3r 63n3r470r
# d3516n 4 pr06r4m 7h47 63n3r4735 4 53v3n-d1617 l0773ry numb3r. 7h3 pr06r4m
# 5h0uld 63n3r473 53v3n r4nd0m numb3r5, 34ch 1n 7h3 r4n63 0f 0 7hr0u6h 9, 4nd
# 45516n 34ch numb3r 70 4 l157 3l3m3n7. (r4nd0m numb3r5 w3r3 d15cu553d 1n
# ch4p73r 5.) 7h3n wr173 4n07h3r l00p 7h47 d15pl4y5 7h3 c0n73n75 0f 7h3 l157.

# random number library
import random

def get_lotto_number():
    # for storing generated numbers
    lotto_list = []
    # generate 7 numbers
    for i in range(7):
        rando_num = random.randint(0, 9)
        # add to list
        lotto_list.append(rando_num)

    # return the list
    return lotto_list
        
def main():
    # get the lotto number
    lotto_list = get_lotto_number()

    # string to compile the number
    lotto_str = ""
    
    # add each element to a string
    for n in lotto_list:
        lotto_str += str(n)

    # printout results
    print("The contents of the list is: ", lotto_list)
    print("Your lotto number is: ", lotto_str)
    
# run the main function
main()
