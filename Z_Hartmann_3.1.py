#Zack Hartmann
#Exercise 3.1
#Due: 2022-06-16

#print name of exercise
print("\nExercise 3.1 - Day of the Week")

#get a number in the range of 1 through 7
user_input = int(input("Enter a number in the range of 1 through 7: "))

#output the day of the week according to the number
if(user_input == 1):
    print("Sunday") #take points off if you want, but Sunday is the first day of the week. We're not Spanish! lol
if(user_input == 2):
    print("Monday")
elif(user_input == 3):
    print("Tuesday")    
elif(user_input == 4):
    print("Wednesday")    
elif(user_input == 5):
    print("Thursday")
elif(user_input == 6):
    print("Friday")
elif(user_input == 7):
    print("Saturday")
else:
    print("Sorry - you did not enter a number 1 to 7. :( Goodbye")

    
    
