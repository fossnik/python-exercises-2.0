# Zack Hartmann
# Exercise 6.7
# Due 2022-07-28

# R@ndom Num6er Fi1e Wri+er
# Wri+e @ pro9r@m +h@+ wri+e5 @ 5erie5 of r@ndom num6er5 +o @ fi1e. 3@ch r@ndom
# num6er 5hou1d 6e in +he r@n9e of 1 +hrou9h 500. 7he @pp1ic@+ion 5hou1d 1e+ +he
# u5er 5pecify how m@ny r@ndom num6er5 +he fi1e wi11 ho1d.

# random number generation library
import random

# path to the file
FILE_PATH = "random_numbers.txt"

def open_my_file(path_string):
    # going to try to open the file; throw exception if needed
    try:
        # open the file (for writing)
        file = open(path_string, "w")

    # will throw error if the given file is not found (or can't be accessed)
    except IOError:
        print("Sorry - we could not open: ", path_string, "(IOError)")

    # return the file parser object for file access
    return file

def ask_user_how_many_numbers_to_generate():
    numbers = 0

    # keep prompting user until they enter a positive number
    while numbers < 1:
        # prompt user - how many numbers should be generated?
        try:
            user_input = input("How many numbers do you want? ")
            # this is where it will fail if user enters non-number
            numbers = int(user_input)
        except:
            print("Invalid input")

    return numbers

# append the numbers to the file
def add_random_numbers_to_file(qty_to_generate_of_numbers, file_object):
    # do the given quantity of numbers
    for i in range(0, qty_to_generate_of_numbers):
        # generate random number in range of 1 through 500
        random_number = random.randint(1, 500)

        # convert to string
        random_number_str = str(random_number)
        
        # add the random number to the file
        file_object.write(random_number_str + "\n")

    # print task completion message for user
    print("Your numbers have completed generation")
    print("See file: ", FILE_PATH)

def main():
    # call my own method for opening the file (will catch an IOError)
    file = open_my_file(FILE_PATH)

    # get valid input from the user for how many numbers to generate
    qty_to_generate_of_numbers = ask_user_how_many_numbers_to_generate()
    
    # append the numbers to the file
    add_random_numbers_to_file(qty_to_generate_of_numbers, file)
    
    # close the file
    file.close()
    
# run the main function
main()
