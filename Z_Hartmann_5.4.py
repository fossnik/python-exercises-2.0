# Zack Hartmann
# Exercise 5.4
# Due 2022-07-14

# Automobile Costs
# Write a program that asks the user to enter the monthly costs for the
# following expenses incurred from operating his or her automobile: loan
# payment, insurance, gas, oil, tires, and maintenance.
# The program should then display the total monthly cost of these expenses,
# and the total annual cost of these expenses.

# List of all the items
LIST_OF_ITEMS = "loan payment", "insurance", "gas", "oil", "tires", "maintenance"

# FUNC - get cost of all items
def get_sum_cost_of_all_items(list_of_items):

    # store sum of items
    sum_of_items = 0.0

    # FUNC - ask user for how much they spent on item
    def how_much_was_spent_on_item(item):
        return float(input(f"How much did you spend on {item}?: "))

    # iterate through list of items, prompt user for each
    for item in list_of_items:
        sum_of_items += how_much_was_spent_on_item(item)

    return sum_of_items


monthly_cost = get_sum_cost_of_all_items(LIST_OF_ITEMS)
annual_cost = monthly_cost * 12

print("\n\t - Automobile Costs -")
print("Monthly: ", monthly_cost)
print("Yearly:  ", annual_cost)
