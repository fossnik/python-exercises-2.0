# Zack Hartmann
# Exercise 5.7
# Due 2022-07-16

# Stadium Seating
# There are three seating categories at a stadium. Class A seats cost $20,
# Class B seats cost $15, and Class C seats cost $10. Write a program that asks
# how many tickets for each class of seats were sold, then displays the amount
# of income generated from ticket sales.

# main function
def main():
    # prompt how many seats sold ($20 each)
    ask_how_many_seats_sold_class_A()

    # prompt how many seats sold ($15 each)
    ask_how_many_seats_sold_class_B()

    # prompt how many seats sold ($10 each)
    ask_how_many_seats_sold_class_C()

    income_from_ticket_sales = ticket_sales_A + ticket_sales_B + ticket_sales_C

    # print out the total sales
    print(f"Total ticket sales: {income_from_ticket_sales}")

# class A seats - prompt user
def ask_how_many_seats_sold_class_A():
    # global variable for ticket sales of "A" Seats
    global ticket_sales_A
    # prompt user - store as int
    qty = int(input("How many Class A tickets were sold?: "))
    # calculate $20 each
    ticket_sales_A = qty * 20
    
# class B seats - prompt
def ask_how_many_seats_sold_class_B():
    # global variable for ticket sales of "B" Seats
    global ticket_sales_B
    # prompt user - store as int
    qty = int(input("How many Class B tickets were sold?: "))
    # calculate $15 each
    ticket_sales_B = qty * 15

# class C seats - prompt
def ask_how_many_seats_sold_class_C():
    # global variable for ticket sales of "C" Seats
    global ticket_sales_C
    # prompt user - store as int
    qty = int(input("How many Class C tickets were sold?: "))
    # calculate $10 each
    ticket_sales_C = qty * 10

# run the main function
main()
