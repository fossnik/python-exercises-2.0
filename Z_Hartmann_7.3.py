# Zack Hartmann
# Exercise 7.3
# Due 2022-07-31

# r41nf4ll 57471571c5
# d3516n 4 pr06r4m 7h47 l375 7h3 u53r 3n73r 7h3 7074l r41nf4ll f0r 34ch 0f 12
# m0n7h5 1n70 4 l157. 7h3 pr06r4m 5h0uld c4lcul473 4nd d15pl4y 7h3 7074l
# r41nf4ll f0r 7h3 y34r, 7h3 4v3r463 m0n7hly r41nf4ll, 7h3 m0n7h5 w17h 7h3
# h16h357 4nd l0w357 4m0un75.

MONTHS_OF_YEAR = ["January", "February", "March", "April", "May", "June", "July"
                  , "August", "September", "October", "November", "December"]

# get the rainfall amounts for each month from user
def get_rainfall_total_for_each_month():
    # validate user input
    def prompt_for_rainfall_validated(month):
        rainfall = -1
        # keep prompting till valid input
        while rainfall < 0:
            try:
                user_input = input(f"How much rain fell in {month}? ")
                # non-numerical input fails here
                rainfall = float(user_input)
            except:
                print("Invalid input")
        return rainfall

    # store rainfall totals in list
    rainfall_totals = []

    # loop through each month and prompt user
    for month in MONTHS_OF_YEAR:
        user_input = prompt_for_rainfall_validated(month)
        rainfall_totals.append(user_input)

    return rainfall_totals

# sum up all the rainfalls totals
def calculate_total_rainfall(rainfall_totals):
    sum_of_rainfall = 0
    
    # loop through each month; add to sum
    for rainfall in rainfall_totals:
        sum_of_rainfall += rainfall
        
    return sum_of_rainfall

# identify the month of least rainfall
def find_least_rainfall(monthly_rainfall_totals):
    # initial case is the rainfall in January; test against that
    least_rainfall = monthly_rainfall_totals[0]
    
    # loop through each month
    for rainfall in monthly_rainfall_totals:
        # save least rainfall amount if less than current record
        if rainfall < least_rainfall:
            least_rainfall = rainfall

    return least_rainfall

# identify the month of most rainfall
def find_most_rainfall(monthly_rainfall_totals):
    # initial case is the rainfall in January; test against that
    most_rainfall = monthly_rainfall_totals[0]
    
    # loop through each month
    for rainfall in monthly_rainfall_totals:
        # save least rainfall amount if more than current record
        if rainfall > most_rainfall:
            most_rainfall = rainfall

    return most_rainfall

# returns list of months that match a given amount
def months_equal_to(given_rainfall, totals):
    ## A list comprehension; returns a list of matching months
    ## unfortunately, this returns values, not indexes (or month names)
    #matching_months = [month for month in totals if month == given_rainfall]

    matching_months = []
    # loop through each month to find matching values
    for idx in range(12):
        if totals[idx] == given_rainfall:
            # add the month name based on index value
            matching_months.append(MONTHS_OF_YEAR[idx])
    
    return matching_months
    
def main():
    # get the rainfall amounts in each month from user
    rainfall_by_month = get_rainfall_total_for_each_month()

    # get the sum of all months of rainfall
    total_rainfall = calculate_total_rainfall(rainfall_by_month)

    # calculate average rainfall
    average_rainfall = total_rainfall / 12
    
    # get most rainfall
    most_rainfall = find_most_rainfall(rainfall_by_month)

    # get all months that match record high
    most_rainfall_months = months_equal_to(most_rainfall, rainfall_by_month)

    # get least rainfall
    least_rainfall = find_least_rainfall(rainfall_by_month)

    # get all months that match record low
    least_rainfall_months = months_equal_to(least_rainfall, rainfall_by_month)

    # display total rainfall for the year
    print("\nTotal rainfall for year: ", total_rainfall)

    # display the average monthly rainfall
    print(f"Average rainfall for year: {average_rainfall:.2f}")

    # display the months with the highest rainfall
    print("\nRecord high rainfall: ", most_rainfall)
    print("Occured in: ", most_rainfall_months)

    # display the months with the lowest rainfall
    print("\nRecord low rainfall: ", least_rainfall)
    print("Occured in: ", least_rainfall_months)
    
# run the main function
main()
