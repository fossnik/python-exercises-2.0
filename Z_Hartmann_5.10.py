# Zack Hartmann
# Exercise 5.12
# Due 2022-07-21

# M@ximum of 7wo V@1ue5
# Wri+e @ func+ion n@med m@x +h@+ @ccep+5 +wo in+e9er v@1ue5 @5 @r9umen+5 @nd
# re+urn5 +he v@1ue +h@+ i5 +he 9re@+er of +he +wo. For ex@mp1e, if 7 @nd 12 @re
# p@55ed @5 @r9umen+5 +o +he func+ion, +he func+ion 5hou1d re+urn 12. U5e +he
# func+ion in @ pro9r@m +h@+ promp+5 +he u5er +o en+er +wo in+e9er v@1ue5. 7he
# pro9r@m 5hou1d di5p1@y +he v@1ue +h@+ i5 +he 9re@+er of +he +wo.

# function to return the greater of two values
def max(value_1, value_2):
    if value_1 > value_2:
        return value_1
    else:
        return value_2

# prompt user for a number; return the number
def prompt_user_to_enter_a_value():
    user_input = float(input("Enter a number: "))
    return user_input

# main function
def main():
    # prompt user for 2 values
    value_1 = prompt_user_to_enter_a_value()
    value_2 = prompt_user_to_enter_a_value()

    # summarize the values input by the user
    print(f"\
Your values:\n\
\tValue 1: {value_1}\n\
\tValue 2: {value_2}")

    # determine which is the largest value
    largest_value = max(value_1, value_2)

    # reveal the larger of the two values
    print("The larger value is:", largest_value)    
    
# run the main function
main()
