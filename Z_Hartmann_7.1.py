# Zack Hartmann
# Exercise 7.1
# Due 2022-07-31

# 7o+@1 $@1e5
# De5i9n @ pro9r@m +h@+ @5k5 +he u5er +o en+er @ 5+ore’5 5@1e5 for e@ch d@y of
# +he week. 7he @moun+5 5hou1d 6e 5+ored in @ 1i5+. U5e @ 1oop +o c@1cu1@+e +he
# +o+@1 5@1e5 for +he week @nd di5p1@y +he re5u1+.

DAYS_OF_WEEK = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
                "Friday", "Saturday"]

# get the sales numbers from the whole week
def get_sales_from_each_day_of_week():
    # input validating function
    def get_valid_positive_number_from_user(day):
        profit = 0
        # keep prompting user until they enter a positive number
        while profit < 1:
            try:
                user_input = input(f"How much profit did you make on {day}? ")
                # this is where it will fail if user enters non-number
                profit = float(user_input)
            except:
                print("Invalid input")
        return profit
    
    # store profits in a list
    profits_list = []
    
    # loop through each day and prompt user for number
    for day in DAYS_OF_WEEK:
        user_input = get_valid_positive_number_from_user(day)
        profits_list.append(user_input)

    return profits_list
    
def main():
    # get the profits from each day of week
    profits = get_sales_from_each_day_of_week()
    
    # keep track of sum
    sum_profit = 0

    # loop through each profit and sum total
    for profit_in_day in profits:
        sum_profit += profit_in_day

    print(f"Your total profit is: ${sum_profit:,.2f}")
    
# run the main function
main()
