# Zack Hartmann
# Exercise 10.2 - Final Project
# Due 2022-08-18

# import the library for random number generation
import random

# constant values
RANDO_NUMBER_FLOOR = 1
RANDO_NUMBER_CEILING = 30
STARTING_LIVES = 5
FILE_PATH_GAMEHISTORY = "gameHistory.txt"

# open a file (in read or write mode depending on arguments)
def open_my_file(path_string, read_or_write_mode):
    # try to open the file; throw exception if needed
    try:
        # open the file
        file = open(path_string, read_or_write_mode)

        # return the file handler object for file access
        return file

    # will throw error if the given file can't be accessed
    except IOError:
        print("Sorry - we could not open: ", path_string, "(IOError)")

# validate user input (positive numbers only)
def prompt_user_for_guess():
    return_value = "null"

    # keep prompting till valid input
    while return_value == "null":
        try:
            user_input = input(f"Enter a guess: ")
            # non-numerical input fails here
            return_value = int(user_input)
        except:
            print("Invalid input")

    return return_value

# Prompt user with query (accept only "Y" or "N" for input)
def get_yes_or_no(user_prompt):
    # loop until acceptable user input triggers return of True or False
    while True:
        # get input from user (based on passed-in prompt)
        user_input = input(user_prompt + " Enter 'Y' or 'N': ")

        # return True based on user input of Y or y
        if user_input == "Y" or user_input == "y":
            return True

        # return False based on user input of N or n
        if user_input == "N" or user_input == "n":
            return False

# Give the player (5) chances to guess a number; return "Win" or "Loss"
def play_guessing_game():
    # generate the secret number
    secret_number = random.randint(RANDO_NUMBER_FLOOR, RANDO_NUMBER_CEILING)

    # set the number of guesses that a user is allowed to make
    remaining_lives = STARTING_LIVES

    # the result of a user guess (null initial value)
    result = "null"

    # a list to store the guesses
    guesses_list = []

    # Debugging purposes - print the secret number
    # print("The Secret Number is (for testing purposes): ", secret_number)

    # prompt user for guesses until lives exhausted (or correct answer entered)
    while remaining_lives > 0:
        # display how many lives remain
        print(f"\nRemaining Guesses: [ {remaining_lives} ]")

        # allow user to enter a guess (input-validating)
        user_guess = prompt_user_for_guess()

        # append the guess to the list
        guesses_list.append(user_guess)

        # evaluate the user guess
        if user_guess < secret_number:
            print("Your guess was TOO LOW")  # print result of guess
            remaining_lives -= 1  # bad guess penalty (lose one life)
        elif user_guess > secret_number:
            print("Your guess was TOO HIGH")  # print result of guess
            remaining_lives -= 1  # bad guess penalty (lose one life)
        else:
            # The game has been won! Print victory message.
            print("\nWINNER!! You have correctly guessed the SECRET NUMBER!\n")
            return "Win"  # return "Win" because game ended in victory

    # Loop exited due to loss of life - indicate that game has ended in failure
    print("\nYou have no remaining guesses. GAME OVER!\n")

    # show the user their guess history
    print("You guessed the following numbers:", guesses_list)

    # reveal the answer
    print(f"The SECRET NUMBER was: {secret_number}\n")

    # return "Loss" because game resulted in a loss
    return "Loss"

def main():
    # print Application Title (The Guessing Game)
    print("You are playing: -The Guessing Game-")

    # print Application Description
    print(f"A *VERY FUN* game where you attempt to guess a number.")

    # print my name, as creator
    print("\tCreated by: Zack Hartmann\n")

    # print directions for the game
    print(f"Directions:\n\
\t- Try to guess our SECRET NUMBER (between \
{RANDO_NUMBER_FLOOR} and {RANDO_NUMBER_CEILING}).\n\
\t- You're allowed a maximum of {STARTING_LIVES} guesses.\n\
\t- You'll be told whether each guess is above or below the SECRET NUMBER.")

    # open a file to record game history
    game_history_file = open_my_file(FILE_PATH_GAMEHISTORY, "w")

    # variable for whether the user wishes to continue
    user_wants_to_play = True

    # count of how many games have been played (for game history file)
    games_played = 0

    # play and replay The Guessing Game until the user decides to quit
    while user_wants_to_play:
        # run the guessing game
        game_result = play_guessing_game()

        # increment count of games played
        games_played += 1

        # record result of game in the game history file
        game_history_file.write(f"Game {games_played}: {game_result} \n")

        # ask user if they want to continue
        user_wants_to_play = get_yes_or_no("Would you like to play a new game?")

    # close the game history file
    game_history_file.close()

    # ask user if they'd like to see their game history
    if get_yes_or_no("Would you like to view your game history?"):
        # open game history file in read mode
        game_history_file = open_my_file(FILE_PATH_GAMEHISTORY, "r")

        # print a game history header
        print("\n --- GAME HISTORY ---")

        # print how many games played
        print("Total games played: ", games_played)

        # get and print text of game history file
        text_of_game_history_file = game_history_file.read()
        print(text_of_game_history_file)

        # close game history file
        game_history_file.close()

    # user is done playing, so thank them. (End of program).
    print("\nThank you for playing -The Guessing Game-")

# run the main function
main()
