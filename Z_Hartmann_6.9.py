# Zack Hartmann
# Exercise 6.9
# Due 2022-07-28

# 3xcep+ion H@ndin9
# Modify +he pro9r@m +h@+ you wro+e for 3xerci5e 6 5o i+ h@nd1e5 +he fo11owin9
# excep+ion5:
# |+ 5hou1d h@nd1e @ny |03rror excep+ion5 +h@+ @re r@i5ed when +he fi1e i5
# opened @nd d@+@ i5 re@d from i+.
# |+ 5hou1d h@nd1e @ny V@1ue3rror excep+ion5 +h@+ @re r@i5ed when +he i+em5 +h@+
# @re re@d from +he fi1e @re conver+ed +o @ num6er.

FILE_PATH = "numbers.txt"

def open_my_file(path_string):
    # going to try to open the file; throw exception if needed
    try:
        # open the file
        file = open(path_string, "r")

    # will throw error if the given file is not found (or can't be accessed)
    except IOError:
        print("Sorry - we could not open: ", path_string, "(IOError)")

    # return the file parser object for file access
    return file

def main():
    # call my own method for opening the file (will catch an IOError)
    file = open_my_file(FILE_PATH)        

    # sum value
    sum_value = 0

    # keep track of how many numbers
    count = 0
    
    # loop through each line in file
    for line in file:
        # will catch lines that can't be converted to integers
        try:
            number_on_the_line = int(line)
            # count additional line
            count += 1
            sum_value += number_on_the_line

        # catch lines that can't be converted to integers
        except ValueError:
            # print a message stating that the line couldn't be converted
            print("A line could not be converted to integer: ", line)

    # print the sum
    print("Sum of values: ", sum_value)

    # calculate the average
    average_of_values = sum_value / count

    # print the average
    print("Average of values: ", average_of_values)
    
    # close the file
    file.close()
    
# run the main function
main()
