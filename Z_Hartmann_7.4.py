# Zack Hartmann
# Exercise 7.4
# Due 2022-08-04

# numb3r 4n4ly515 pr06r4m
# d3516n 4 pr06r4m 7h47 45k5 7h3 u53r 70 3n73r 4 53r135 0f 20 numb3r5. 7h3
# pr06r4m 5h0uld 570r3 7h3 numb3r5 1n 4 l157 7h3n d15pl4y 7h3 f0ll0w1n6 d474:
#  7h3 l0w357 numb3r 1n 7h3 l157
#  7h3 h16h357 numb3r 1n 7h3 l157
#  7h3 7074l 0f 7h3 numb3r5 1n 7h3 l157
#  7h3 4v3r463 0f 7h3 numb3r5 1n 7h3 l157

# validate user input (numbers only)
def get_a_number():
    return_value = "null"
    # keep prompting till valid input
    while return_value == "null":
        try:
            user_input = input(f"Enter a number: ")
            # non-numerical input fails here
            return_value = float(user_input)
        except:
            print("Invalid input")
    return return_value
    
# get 20 positive numbers
def get_twenty_numbers():
    # to store the 20 values
    twenty_numbers = []
    
    # prompt user for 20 numbers
    for number in range(20):
        user_input = get_a_number()
        twenty_numbers.append(user_input)

    return twenty_numbers

# sum up all the numbers
def sum_all_numbers(numbers_list):
    sum_of_numbers = 0
    
    # loop through each month; add to sum
    for num in numbers_list:
        sum_of_numbers += num

    return sum_of_numbers

# identify the most or least in a list
def find_most_or_least(number_list, most_or_least):
    # :input number_list: list - twenty numerical values
    # :input most_or_least: string - which value is sought? highest or lowest?
    
    # initial case is the first index in the list; test against that
    superlative_value = number_list[0]
    
    # loop through values; find the most superlative (highest or lowest)
    for num in number_list:
        if most_or_least == "most" and num > superlative_value \
           or most_or_least == "least" and num < superlative_value:
            superlative_value = num
            
    return superlative_value
    
def main():
    # get twenty numbers from user
    numbers_list = get_twenty_numbers()

    # get and display LOWEST number
    lowest_number = find_most_or_least(numbers_list, "least")
    print("\nLowest value: ", lowest_number)
    
    # get and display HIGHEST number
    highest_number = find_most_or_least(numbers_list, "most")
    print("Highest value: ", highest_number)

    # calculate and display the TOTAL of all the numbers in the list
    sum_of_numbers = sum_all_numbers(numbers_list)
    print("Sum total: ", sum_of_numbers)

    # calculate and display the AVERAGE of all numbers in the list
    average_of_numbers = sum_of_numbers / len(numbers_list)
    print(f"Average: {average_of_numbers:.2f}")

# run the main function
main()
