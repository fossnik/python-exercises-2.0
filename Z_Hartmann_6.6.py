# Zack Hartmann
# Exercise 6.6
# Due 2022-07-21

# 4ver@9e of Num6er5
# 455ume @ fi1e con+@inin9 @ 5erie5 of in+e9er5 i5 n@med num6er5.+x+ @nd exi5+5
# on +he compu+er’5 di5k. Wri+e @ pro9r@m +h@+ c@1cu1@+e5 +he @ver@9e of @11 +he
# num6er5 5+ored in +he fi1e.

def main():
    # open the file
    file = open("numbers.txt", "r")

    # sum value
    sum_value = 0

    # keep track of how many numbers
    count = 0
    
    # loop through each line in file
    for line in file:
        # count additional line
        count += 1
        sum_value += int(line)

    # print the sum
    print("Sum of values: ", sum_value)

    # calculate the average
    average_of_values = sum_value / count

    # print the average
    print("Average of values: ", average_of_values)
    
    # close the file
    file.close()
    
# run the main function
main()
