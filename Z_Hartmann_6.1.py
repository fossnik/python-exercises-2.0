# Zack Hartmann
# Exercise 6.1
# Due 2022-07-23

# Fi1e Di5p1@y
# 455ume @ fi1e con+@inin9 @ 5erie5 of in+e9er5 i5 n@med num6er5.+x+ @nd exi5+5
# on +he compu+er’5 di5k. Wri+e @ pro9r@m +h@+ di5p1@y5 @11 of +he num6er5 in
# +he fi1e.

def main():
    # Open file in read-only mode
    file = open("numbers.txt", "r")

    # create a line reader
    line = file.readline()

    # loop through each line of the file
    while line != "":
        # print that line
        print(line)
        # read the next line
        line = file.readline()

    # close the file
    file.close()
    
# run the main function
main()
