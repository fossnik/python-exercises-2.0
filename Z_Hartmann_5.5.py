# Zack Hartmann
# Exercise 5.5
# Due 2022-07-16

# Property Tax
# A county collects property taxes on the assessment value of property, which
# is 60 percent of the property’s actual value. For example, if an acre of
# land is valued at $10,000, its assessment value is $6,000. The property tax
# is then 72¢ for each $100 of the assessment value. The tax for the acre
# assessed at $6,000 will be $43.20. Write a program that asks for the actual
# value of a piece of property and displays the assessment value and property
# tax.

# main function
def main():
    # prompt user for property actual value
    ask_for_actual_value()
    # display the assessment value
    display_assessment_value()
    # display the property tax
    display_property_tax()

def ask_for_actual_value():
    # this global variable can be used in other functions
    global actual_value
    # prompt user for property value; store as float (global)
    actual_value = float(input("What is the value of the property?: "))

def display_assessment_value():
    global assessment_value
    # calculate the assessment value based on 60%
    assessment_value = actual_value * 0.60
    # print assessment value; formatted as money
    print(f"Assessment Value: {assessment_value:,.2f}")

def display_property_tax():
    # calculate the property tax based on 7.2%
    property_tax = assessment_value * 0.0072
    # print the property tax; formatted as money
    print(f"Property Tax: {property_tax:,.2f}")

main()
