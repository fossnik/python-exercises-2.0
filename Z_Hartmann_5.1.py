# Zack Hartmann
# Exercise 5.1
# Due 2022-07-14

# Assignment Information
# Kilometer Converter?
# Write a program that asks the user to enter a distance in kilometer, then
# converts that distance to miles. The conversion formula is as follows:
#  Miles = kilometers X 0.6214

# FUNCTION - prompt user for kilometers
def getUserInput():
    user_input = float(input("How many kilometers?: "))
    return user_input

# FUNCTION - convert the kilometers into miles
def convertKilometersToMiles(miles):
    return miles * 0.6214

# call func to get user input
user_input = getUserInput()

# convert it into miles / store value
distance_in_miles = convertKilometersToMiles(user_input)

# print the output
print(f"{user_input} kilometers is: {distance_in_miles:.4} miles")
